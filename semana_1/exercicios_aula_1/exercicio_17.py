"""
17.

Faça um programa, com uma função, que calcula a mediana de uma lista.

```
Funções embutidas que podem te ajudar:
  - sorted(lista) -> ordena a lista
```
"""

lista = [1, 2, 4, 3]


def mediana(Lista: list):
    """Short summary.

    Parameters
    ----------
    Lista : list
        Lista de numeros inteiros.

    Returns
    -------
    type
        Retorna a mediana entre os numeros da Lista.

    """
    Lista = sorted(Lista)
    meio = float(len(Lista) / 2)
    if(meio % 2 != 0):
        return Lista[int((meio / 2) - .5)]
    else:
        return (Lista[int((meio))] + Lista[int((meio)-1)])/2


print(mediana(lista))
