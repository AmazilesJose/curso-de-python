"""
21.

Dada uma lista de entradas de usuário de números inteiros,
construa um dicionário com a lista padrão,
a lista dos valores elevados ao quadrado e a lista dos valores elevados ao cubo
"""

lista = [1, 2, 3, 4, 5, 6]


def padrão(Lista: list):
    """Função que retorna a Lista.

    Parameters
    ----------
    Lista : list
        Lista de inteiros.

    Returns
    -------
    type
        Retorna a Lista.

    """
    return Lista


def quadrado(Lista: list):
    """Eleva ao quadrado todo numero de uma lista.

    Parameters
    ----------
    Lista : list
        Lista de inteiros.

    Returns
    -------
    type
        Retorna uma lista com os elementos ao quadrado da Lista.

    """
    return list(map(lambda x: x**2, Lista))


def cubo(Lista: list):
    """Eleva ao cubo todo numero de uma lista.

    Parameters
    ----------
    Lista : list
        Lista de inteiros.

    Returns
    -------
    type
        Retorna uma lista com os elementos ao cubo da Lista.


    """
    return list(map(lambda x: x**3, Lista))


def mexer_na_lista(funcao, Lista: list):
    """Contem um dicionario com as funções e fornece a função requerida.

    Parameters
    ----------
    funcao : type
        Função inputada e que vai ser retornada.
    Lista : list
        Lista de inteiros.

    Returns
    -------
    type
        Retorna uma lista que foi passada para a função inputuda.

    """
    funções = {
            'padrão': padrão,
            'quadrado': quadrado,
            'cubo': cubo
    }
    return funções[funcao](Lista)


print(mexer_na_lista('padrão', lista))
