"""
20.

Baseando-se nos exercícios passados,
monte um dicionário com os seguintes seguintes chaves:

`lista, somatório, tamanho, maior valor e menor valor
`
"""

lista = [1, 2, 3, 4]
dicionario = {
    'lista': lista,
    'somatorio': sum(lista),
    'tamanho': len(lista),
    'maior': max(lista),
    'menor': min(lista)
    }
print(dicionario)
