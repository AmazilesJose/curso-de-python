"""
16.

Faça um programa, com uma função, que calcula a média de uma lista.

```
Funções embutidas que podem te ajudar:
  - len(lista) -> calcula o tamanho da lista
  - sum(lista) -> faz o somatório dos valores
```
"""

lista = [1, 2, 3, 4]


def media(Lista: list):
    """Calcula a media de uma Lista fornecida.

    Parameters
    ----------
    Lista : list
        Lista de numeros inteiros.

    Returns
    -------
    type
        Media entre os numeros dentro da Lista.

    """
    return sum(Lista)/len(Lista)


print(media(lista))
