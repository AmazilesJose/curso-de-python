"""
7.

Faça um programa que receba uma string e responda se ela tem alguma vogal,
se sim, quais são? E também diga se ela é uma frase ou não.
"""

string = input('Digite a string: ')
vogal = 0
frase = 0
for letra in string:
    if(letra in 'aeiou'):
        vogal = 1
    elif(letra == ' '):
        frase = 1
if(vogal == 1):
    print('Tem vogal')
if(frase == 1):
    print('É uma frase')
