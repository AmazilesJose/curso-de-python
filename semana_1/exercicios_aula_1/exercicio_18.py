"""
18.

Faça um programa, com uma função que dado uma lista
e uma posição da mesma faça o quartil dessa posição.

`p_index = int(p * len(lista))`
"""

lista = [1, 23, 43, 35, 656, 34, 46, 87, 123, 54]


def quartil(Lista: list, p: float):
    """Short summary.

    Parameters
    ----------
    Lista : list
        Lista de numeros inteiros.
    p : float
        Porcentagem ao qual o quartil se refere.

    Returns
    -------
    type
        Retorna o quartil requisitado.

    """
    return sorted(Lista)[int(p * len(Lista))]


print(quartil(lista, 0.5))
