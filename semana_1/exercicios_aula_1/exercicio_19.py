"""
19.

Faça um programa, com uma função, que calcule a dispersão de uma lista

```
Funções embutidas que podem te ajudar:
  - min(lista) -> retorna o menor valor
  - max(lista) -> retorna o maior valor
```

"""

lista = [1, 2, 3, 4]


def dispersao(Lista: list):
    """Função que calcula a dispersão de numeros fornecidos por uma Lista.

    Parameters
    ----------
    Lista : list
        Lista de numeros inteiros.

    Returns
    -------
    type
        Retorna a Dispersão ad Lista fornecida.

    """
    return max(Lista) - min(Lista)


print(dispersao(lista))
