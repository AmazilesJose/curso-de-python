"""
11.
Faça um programa que itere em uma string e
toda vez que uma vogal aparecer na sua string print o seu nome entre as letras

 Exemplo:
        ```
        string = bananas
        b
        eduardo
        n
        eduardo
        n
        ...
        ```
"""

string = input('Digite a string: ')
for letra in string:
    if letra not in 'aeiou':
        print(letra)
    else:
        print('amaziles')
