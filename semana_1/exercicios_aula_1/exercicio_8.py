"""
8.

Faça um programa que receba uma data de nascimento (15/07/87) e imprima
```
Você nasceu em <dia> de <mes> de <ano>
```
"""

data = (input('Digita a data de nascimento: ')).split('/')

print('Você nasceu em {0} de {1} de {2}'.format(data[0], data[1], data[2]))
