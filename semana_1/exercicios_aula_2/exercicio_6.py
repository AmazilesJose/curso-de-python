"""
6.

Usando os mesmos inputs do código anterior,
reconstrua o problema de maneira declarativa.

```
Funções que pode te ajudar:
    - operator.mul
    - map()
```
"""


def funcao(Lista: list):
    """Pega um Lista e duplica seus elementos.

    Parameters
    ----------
    Lista : list
        Lista de string.

    Returns
    -------
    type
        Retorna uma lista com os elementos da Lista duplicados.

    """
    return list(map(lambda x: x*2, Lista))


# print(funcao(lista))
