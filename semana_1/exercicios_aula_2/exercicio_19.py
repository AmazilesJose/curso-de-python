"""
19.

Dada uma lista:
```
[‘keep’, ‘remove’, ‘keep’, ‘remove’, ‘keep’, ‘remove’]
```
Usando a função filter remova tudo que deve ser removido
"""


def filtrar(Lista: list):
    return list(filter(lambda x: x != 'remove', Lista))
    # return [elemento * 2 for elemento in lista if elemento != 'remove']
