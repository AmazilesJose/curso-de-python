"""
15.

Faça uma função que se aplique uma função duas vezes em um valor passado

```
reaplica(soma_2, 2) -> 6
reaplica(sub_2, 2) -> -2
```
"""


def soma_2(valor: int):
    return valor + 2


def sub_2(valor: int):
    return valor - 2


def reaplica(funcao, valor: int):
    func = {
    'soma_2': soma_2,
    'sub_2': sub_2
    }
    
    return func[funcao](func[funcao](valor))


# print(reaplica(soma_2, 2))
# print(reaplica(sub_2, 2))
