"""
14.

Faça uma função que receba um número, caso esse número seja múltiplo de 3 e 5,
retorne “romeu e julieta”, caso contrário, retorne o valor de entrada.

```
f(3) -> 3
f(5) -> 5
f(15) -> ‘romeu e juleita’
```
"""


def romjulão(numero: int):
    return 'romeu e julieta' if numero % 15 == 0 else numero


# print(romjulão(3))
