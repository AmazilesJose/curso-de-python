"""
12.

Faça uma função que receba um número, caso esse número seja múltiplo de 3,
retorne “queijo”, caso contrário, retorne o valor de entrada.

```
f(5) -> 5
f(3) -> ‘queijo’
f(6) -> ‘queijo’
```
"""


def queijão(numero: int):
    return 'queijo' if numero % 3 == 0 else numero


# print(queijão(3))
