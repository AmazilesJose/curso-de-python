"""
5.

Faça um programa que itere em uma lista de maneira imperativa
e que armazene em uma nova lista seu valor processado por uma
função:

```
entrada = [‘foo’, ‘bar’, ‘spam’, ‘eggs’]
função = f(x) = x * 2
saida = [‘foofoo’, ‘barbar’ ‘spamspam’, ‘eggseggs’]
```
"""

lista = ['foo', 'bar', 'spam', 'eggs']
lista2 = []
for elemento in lista:
    lista2.append(elemento * 2)

# print(lista2)


def retorno_exercicio_5():
    lista = ['foo', 'bar', 'spam', 'eggs']
    lista2 = []
    for elemento in lista:
        lista2.append(elemento * 2)
    return lista2
