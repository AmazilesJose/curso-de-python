"""
18.

Usando as funções criadas nos exercícios 13, 14 e 15.
Crie uma composição de funções que resolva os 3 casos:

```
EX:
f(x) -> Union[ID, goiabada, queijo, romeu e julieta]
f(3) -> ‘queijo’
f(5) -> ‘goiabada’
f(15) -> ‘romeu e julieta’
f(19) -> 19
```
"""

from typing import Any, Callable
from functools import wraps


def is_int(func: Callable) -> Callable:
    # @wraps(func)
    def inner(val: Any) -> Any:
        if isinstance(val, int):
            return func(val)
        else:
            return val

    inner.__name__ = func.__name__
    inner.__repr__ = func.__repr__
    inner.__str__ = func.__str__
    return inner


"""
-------------------------
"""



def queijão_1(numero: int):
    return 'queijo' if numero % 3 == 0 else numero


queijão = is_int(queijão_1)


@is_int
def goiabão(numero: int):
    return 'goiabada' if numero % 5 == 0 else numero


@is_int
def romjulão(numero: int):
    return 'romeu e julieta' if numero % 15 == 0 else numero


def nova(val: int):
    return queijão(goiabão(romjulão(val)))


def sumario(funcao: str, numero: int):
    funções = {
            'ID': nova,
            'queijão': queijão,
            'goiabão': goiabão,
            'romjulão': romjulão
    }
    return funções[funcao](numero)


# print(sumario('ID', 15))
# print(sumario('queijão', 5))
# print(sumario('goiabão', 3))
# print(sumario('romjulão', 1))
# # print(queijão)
# # print(queijão_1.__code__)

# import pdb; pdb.set_trace()
