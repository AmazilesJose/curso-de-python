import exercicio_5, exercicio_6, exercicio_7, exercicio_8, exercicio_9, exercicio_10
import exercicio_11, exercicio_12, exercicio_13, exercicio_14, exercicio_15, exercicio_16
import exercicio_17, exercicio_18, exercicio_19, exercicio_20, exercicio_21

class Retorno_exercicios:

    def retorna_ex_5(self):
        return exercicio_5.retorno_exercicio_5()

    def retorna_ex_6(self, lista):
        return exercicio_6.funcao(lista)

    def retorna_ex_7(self, lista):
        return exercicio_7.inverter(lista)

    def retorna_ex_8(self, lista):
        return exercicio_8.inverter(lista)

    def retorna_ex_9(self, saudacao, nome):
        return exercicio_9.f(saudacao, nome)

    def retorna_ex_10(self, numero, string):
        return exercicio_10.checar_string(numero, string)

    # def retorna_ex_11(self, variavel):
    #     return exercicio_11.funcao(variavel)

    def retorna_ex_12(self, numero):
        return exercicio_12.queijão(numero)

    def retorna_ex_13(self, numero):
        return exercicio_13.goiabão(numero)

    def retorna_ex_14(self, numero):
        return exercicio_14.romjulão(numero)

    def retorna_ex_15(self, funcao, valor):
        return exercicio_15.reaplica(funcao, valor)

    def retorna_ex_16(self, funcao, numero):
        return exercicio_16.aplica_em_lote(funcao, numero)

    def retorna_ex_17(self, funcao, numero):
        return exercicio_17.aplica(funcao, numero)

    def retorna_ex_18(self, funcao, numero):
        return exercicio_18.sumario(funcao, numero)

    def retorna_ex_19(self, lista):
        return exercicio_19.filtrar(lista)

    def retorna_ex_20(self, lista):
        return exercicio_20.pra_maiscula(lista)

    def retorna_ex_21(self, lista):
        return exercicio_21.somar_elementos(lista)
