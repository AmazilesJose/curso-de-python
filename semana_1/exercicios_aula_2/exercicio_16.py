"""
16.

Faça uma função que receba uma função e um número.
Ela deve retornar uma lista com todos os números em ordem crescente
até aquele valor:

```
aplica_em_lote(soma_1, 3) -> [1, 2, 3, 4]
DICA:
    O index inicia em 0
```
"""


def soma_1(valor: int):
    return valor + 1


def aplica_em_lote(funcao: str, valor: int):
    func = {
    'soma_1': soma_1
    }
    return [func[funcao](numero) for numero in range(0, valor)]
