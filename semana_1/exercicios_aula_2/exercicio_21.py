"""
21.

Dada uma lista:
```
[‘keep’, ‘remove’, ‘keep’, ‘remove’, ‘keep’, ‘remove’]
```
Usando a função reduce faça a somatória de todos os elementos da lista juntos

```
[‘a’, ‘abc’, ‘def’] -> 7
```
"""

from functools import reduce


def somar_elementos(Lista: list):
    return reduce(lambda x, y: x + len(y), Lista, 0)


# print(somar_elementos(lista))
