"""
8.

Resolva de maneira imutável (Ou seja, retornando uma nova lista)

```
Entrada = [1, 2, 3]
Saída = [3, 2, 1]
Coisas que podem te ajudar:
list.insert, list.append, list.remove
```
"""


def inverter(Lista: list):
    nova_lista = []
    for elemento in Lista:
        nova_lista.insert(0, elemento)
    return nova_lista


# print(inverter(lista))
