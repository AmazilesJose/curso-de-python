"""
20.

Dada uma lista:
```
[‘keep’, ‘remove’, ‘keep’, ‘remove’, ‘keep’, ‘remove’]
```
Usando a função map transforme o primeiro carácter em maiúsculo.
"""


def pra_maiscula(Lista: list):
    return list(map(lambda x: x.capitalize(), Lista))


# print(pra_maiscula(lista))
