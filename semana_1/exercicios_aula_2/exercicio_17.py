"""
17.

Faça uma função que receba uma função e uma lista de números,
aplique essa função a todos os valores da lista
e retorne uma nova lista com os valores processados:

```
aplica(func, list) -> [func(list[0]), func(list[n]), ..]
aplica(soma_1, [1, 2, 3]) -> [2, 3, 4]
aplica(sub_1, [1, 2, 3]) -> [0, 1, 2]
```
"""


def soma_1(Lista: list):
    return list(map(lambda x: x + 1, Lista))


def sub_1(Lista: list):
    return list(map(lambda x: x - 1, Lista))


def aplica(funcao, Lista: list):
    func = {
    'soma_1': soma_1,
    'sub_1': sub_1
    }
    return func[funcao](Lista)


# print(aplica(soma_1, lista))
