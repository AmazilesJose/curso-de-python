"""
7.

Faça um programa que faça a inversão de uma lista
usando as propriedade mutáveis dessa lista (remova da lista e insira de novo)

```
Entrada = [1, 2, 3]
Saída = [3, 2, 1]

Coisas que podem te ajudar:
list.insert, list.append, list.remove
```
"""


def inverter(Lista: list):
    """Inverte uma lista fornecida.

    Parameters
    ----------
    Lista : list
        Lista de inteiros.

    Returns
    -------
    type
        Retorna a Lista revertida

    """
    return list(reversed(Lista))


# print(inverter(lista))
