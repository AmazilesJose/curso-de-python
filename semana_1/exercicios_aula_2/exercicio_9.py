"""
9.
Crie uma função que faça uma saudação a alguém.
A função deve receber dois argumentos ‘saudação’ e ‘nome’.

```
f(‘Ahoy’, ‘Fausto’) -> ‘Ahoy Fausto’
f(‘Olá’, bb’) -> Olá bb’
```
"""


def f(saudacao: str, nome: str):
    return (saudacao + ' ' + nome)


# print(f('ola', 'bb'))
