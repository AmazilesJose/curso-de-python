"""
13.

Faça uma função que receba um número, caso esse número seja múltiplo de 5,
retorne “goiabada”, caso contrário, retorne o valor de entrada.

```
f(5) -> ‘goiabada’
f(3) -> 3
f(10) -> ‘goiabada’
```
"""


def goiabão(numero: int):
    return 'goiabada' if numero % 5 == 0 else numero


# print(goiabão(3))
