from unittest import TestCase, mock, main, TestLoader, TestSuite, TextTestRunner
# from
from doc_tests import Retorno_exercicios

def setUpModule():
    print('\nTestando Exercicios com e sem Mock')

def tearDownModule():
    print('\n\nTestes Finalizados')

class TestandoExercicio5(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 5 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 5 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()

    def test_exercicio_5(self):
        saida = ['foofoo', 'barbar', 'spamspam', 'eggseggs']
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_5(), saida, '\n\tExercicio 5 Falhou')

class TestandoExercicio6(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 6 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 6 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()

    def test_exercicio_6(self):
        entrada = ['foo', 'bar', 'spam', 'eggs']
        saida = ['foofoo', 'barbar', 'spamspam', 'eggseggs']
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_6(entrada), saida, '\n\tExercicio 6 Falhou')

class TestandoExercicio7(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 7 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 7 - Terminado')
        print('\n----------------------------------------------------------------------')
    def setUp(self):
        self.c = Retorno_exercicios()

    def test_exercicio_7(self):
        entrada = [1, 2, 3]
        saida = [3, 2, 1]
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_7(entrada), saida, '\n\tExercicio 7 Falhou')

class TestandoExercicio8(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 8 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 8 - Terminado')
        print('\n----------------------------------------------------------------------')
    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_8(self):
        entrada = [1, 2, 3]
        saida = [3, 2, 1]
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_8(entrada), saida, '\n\tExercicio 8 Falhou')

class TestandoExercicio9(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 9 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 9 - Terminado')
        print('\n----------------------------------------------------------------------')
    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_9(self):
        entrada_1 = 'Olá'
        entrada_2 = 'bb'
        saida = 'Olá bb'
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_9(entrada_1, entrada_2), saida, '\n\tExercicio 9 Falhou')

class TestandoExercicio10(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 10 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 10 - Terminado')
        print('\n----------------------------------------------------------------------')
    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_10(self):
        entrada_1 = 3
        entrada_2 = 'aaa'
        saida = True
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_10(entrada_1, entrada_2), saida, '\n\tExercicio 10 Falhou')

    def test_exercicio_10_com_4_retorna_false(self):
        entrada_1 = 4
        entrada_2 = 'aaa'
        saida = False
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_10(entrada_1, entrada_2), saida, '\n\tExercicio 10 Falhou')

# class TestandoExercicio11(TestCase):
#     @classmethod
#     def setUpClass(cls):
#         print('----------------------------------------------------------------------')
#         print('\n  Teste Exercicio 11 - Começado')
#     @classmethod
#     def tearDownClass(cls):
#         print('\n  Teste Exercicio 11 - Terminado')
#         print('\n----------------------------------------------------------------------')
#     def setUp(self):
#             self.c = Retorno_exercicios()
#     def test_exercicio_5(self):
#         saida = ['foofoo', 'barbar', 'spamspam', 'eggseggs']
#         print('\n\tTestando ...')
#         self.assertEqual(self.c.retorna_ex_5(), saida, '\n\tExercicio 5 Falhou')

class TestandoExercicio12(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 12 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 12 - Terminado')
        print('\n----------------------------------------------------------------------')
    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_12_com_1_retorna_1(self):
        entrada = 1
        saida = 1
        print('\n\tTestando dado 1 retorna 1 ...')
        self.assertEqual(self.c.retorna_ex_12(entrada), saida, '\n\tExercicio 12 Falhou')

    def test_exercicio_12_com_3_retorna_queijo(self):
        entrada = 3
        saida = 'queijo'
        print('\n\tTestando dado 3 retorna queijo ...')
        self.assertEqual(self.c.retorna_ex_12(entrada), saida, '\n\tExercicio 12 Falhou')

class TestandoExercicio13(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 13 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 13 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_13_com_1_retorna_1(self):
        entrada = 1
        saida = 1
        print('\n\tTestando dado 1 retorna 1...')
        self.assertEqual(self.c.retorna_ex_13(entrada), saida, '\n\tExercicio 13 Falhou')

    def test_exercicio_13_com_5_retorna_goiabada(self):
        entrada = 5
        saida = 'goiabada'
        print('\n\tTestando  dado 5 retorna goiabada ...')
        self.assertEqual(self.c.retorna_ex_13(entrada), saida, '\n\tExercicio 13 Falhou')

class TestandoExercicio14(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 14 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 14 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_14_com_1_retorna_1(self):
        entrada = 1
        saida = 1
        print('\n\tTestando dado 1 retorna 1 ...')
        self.assertEqual(self.c.retorna_ex_14(entrada), saida, '\n\tExercicio 14 Falhou')

    def test_exercicio_14_com_15_retorna_romeu_e_julieta(self):
        entrada = 15
        saida = 'romeu e julieta'
        print('\n\tTestando dado 15 retorna romeu e julieta ...')
        self.assertEqual(self.c.retorna_ex_14(entrada), saida, '\n\tExercicio 14 Falhou')

class TestandoExercicio15(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 15 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 15 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_15_testando_soma_2(self):
        entrada_1 = 'soma_2'
        entrada_2 = 2
        saida = 6
        print('\n\tTestando soma_2 ...')
        self.assertEqual(self.c.retorna_ex_15(entrada_1, entrada_2), saida, '\n\tExercicio 15 soma_2 Falhou')

    def test_exercicio_15_testando_sub_2(self):
        entrada_1 = 'sub_2'
        entrada_2 = 2
        saida = -2
        print('\n\tTestando sub_2 ...')
        self.assertEqual(self.c.retorna_ex_15(entrada_1, entrada_2), saida, '\n\tExercicio 15 sub_2 Falhou')

class TestandoExercicio16(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 16 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 16 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_16(self):
        entrada_1 = 'soma_1'
        entrada_2 = 3
        saida = [1, 2, 3]
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_16(entrada_1, entrada_2), saida, '\n\tExercicio 16 Falhou')

class TestandoExercicio17(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 17 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 17 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_17_testando_soma_1(self):
        entrada_1 = 'soma_1'
        entrada_2 = [1, 2, 3]
        saida = [2, 3, 4]
        print('\n\tTestando soma_1 ...')
        self.assertEqual(self.c.retorna_ex_17(entrada_1, entrada_2), saida, '\n\tExercicio 17 soma_1 Falhou')

    def test_exercicio_17_testando_sub_1(self):
        entrada_1 = 'sub_1'
        entrada_2 = [1, 2, 3]
        saida = [0, 1, 2]
        print('\n\tTestando sub_1 ...')
        self.assertEqual(self.c.retorna_ex_17(entrada_1, entrada_2), saida, '\n\tExercicio 17 sub_1 Falhou')

class TestandoExercicio18(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 18 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 18 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()

    def test_exercicio_18_testando_ID_retornar_numero(self):
        entrada_1 = 'ID'
        entrada_2 = 1
        saida = 1
        print('\n\tTestando ID dado 1 retornar 1 ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 ID retornar numero Falhou')
    def test_exercicio_18_testando_ID_retornar_queijo(self):
        entrada_1 = 'ID'
        entrada_2 = 3
        saida = 'queijo'
        print('\n\tTestando ID dado 3 retornar queijo ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 ID retornar queijo Falhou')

    def test_exercicio_18_testando_ID_retornar_goiabada(self):
        entrada_1 = 'ID'
        entrada_2 = 5
        saida = 'goiabada'
        print('\n\tTestando ID dado 5 retornar goiabada ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 ID retornar goiabada Falhou')

    def test_exercicio_18_testando_ID_retornar_romeu_e_julieta(self):
        entrada_1 = 'ID'
        entrada_2 = 15
        saida = 'romeu e julieta'
        print('\n\tTestando ID dado 15 retornar romeu e julieta ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 ID retornar romeu e julieta Falhou')

    def test_exercicio_18_testando_queijao_retornar_numero(self):
        entrada_1 = 'queijão'
        entrada_2 = 1
        saida = 1
        print('\n\tTestando queijão dado 1 retornar 1 ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 queijão retornar numero Falhou')

    def test_exercicio_18_testando_queijao_retornar_queijo(self):
        entrada_1 = 'queijão'
        entrada_2 = 3
        saida = 'queijo'
        print('\n\tTestando queijão dado 3 retornar queijo ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 queijão retornar queijo Falhou')

    def test_exercicio_18_testando_goiabao_retornar_numero(self):
        entrada_1 = 'goiabão'
        entrada_2 = 1
        saida = 1
        print('\n\tTestando goiabão dado 1 retornar 1 ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 goiabão retornar numero Falhou')

    def test_exercicio_18_testando_goiabao_retornar_goiabada(self):
        entrada_1 = 'goiabão'
        entrada_2 = 5
        saida = 'goiabada'
        print('\n\tTestando goiabão dado 5 retornar goiabada ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 goiabão retornar goiabada Falhou')

    def test_exercicio_18_testando_romjulao_retornar_numero(self):
        entrada_1 = 'romjulão'
        entrada_2 = 1
        saida = 1
        print('\n\tTestando romjulão dado 1 retornar 1 ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 romjulão retornar numero Falhou')

    def test_exercicio_18_testando_romjulao_retornar_romeu_e_julieta(self):
        entrada_1 = 'romjulão'
        entrada_2 = 15
        saida = 'romeu e julieta'
        print('\n\tTestando romjulão dado 15 retornar romeu e julieta ...')
        self.assertEqual(self.c.retorna_ex_18(entrada_1, entrada_2), saida, '\n\tExercicio 18 romjulão retornar romeu e julieta Falhou')

class TestandoExercicio19(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 19 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  TesteTeste Exercicio 19 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_19(self):
        entrada = ['keep', 'remove', 'keep', 'remove', 'keep']
        saida = ['keep', 'keep', 'keep']
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_19(entrada), saida, '\n\tExercicio 19 Falhou')

class TestandoExercicio20(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 20 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 20 - Terminado')
        print('\n----------------------------------------------------------------------')

    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_20(self):
        entrada = ['keep', 'remove', 'keep', 'remove', 'keep']
        saida = ['Keep', 'Remove', 'Keep', 'Remove', 'Keep']
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_20(entrada), saida, '\n\tExercicio 20 Falhou')

class TestandoExercicio21(TestCase):
    @classmethod
    def setUpClass(cls):
        print('----------------------------------------------------------------------')
        print('\n  Teste Exercicio 21 - Começado')

    @classmethod
    def tearDownClass(cls):
        print('\n  Teste Exercicio 21 - Terminado')
        print('\n----------------------------------------------------------------------')


    def setUp(self):
            self.c = Retorno_exercicios()
    def test_exercicio_21(self):
        entrada = ['keep', 'remove', 'keep', 'remove', 'keep']
        saida = 24
        print('\n\tTestando ...')
        self.assertEqual(self.c.retorna_ex_21(entrada), saida, '\n\tExercicio 21 Falhou')

# ----------------------------------------------------------------------------#
def testLoader():
    return TestLoader().discover('.')


if __name__ == '__main__':
    TextTestRunner().run(testLoader())
    # main(verbosity = 2, exit = False)
