"""
10.
Especifique uma função que receba um número uma string
e retorne se na string há o mesmo número de elementos
que foram passados no parâmetro.

```
f(3, ‘aaa’) -> True
f(10, ‘Batata’) -> False
```
"""


def checar_string(numero: int, string: str):
    if(len(string) == numero):
        return True
    else:
        return False


# print(checar_string(3, 'aaa'))
