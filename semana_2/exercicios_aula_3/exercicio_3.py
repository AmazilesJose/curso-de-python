"""
3.

Dado o código:

```Python
class Calc:
  def add(self, x, y):
    return x + y

  def sub(self, x, y):
    return x + y

  def mult(self, x, y):
    return x + y

  def div(self, x, y):
    return x + y
```

Faça assertivas em relação ao que funciona e não funciona nos seus casos de teste
"""

from doc_exercicios import Calc_3_ao_5

assert Calc_3_ao_5().add(0, 0) == 0
assert Calc_3_ao_5().sub(0, 0) == 0
assert Calc_3_ao_5().mult(0, 0) == 0
assert Calc_3_ao_5().div(1, 1) == 2
