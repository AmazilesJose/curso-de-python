"""
8.

Agora que você já contruiu a função ‘exp’
    que tal fazer uma chamada de uma expressão mais complexa?

  `f(x, y, z) -> exp(exp(x, y, z), y, z)``

  Agora tente efetuar os testes dessa função
"""

from unittest import TestCase
from doc_exercicios import Calc_7_ao_11


class TestExercicio8(TestCase):
    def setUp(self):
        self.c = Calc_7_ao_11()

    def test_exercicio_8(self):
        print('\nTestando Exercicio 8')
        self.assertEqual(self.c.exp(
            self.c.exp(1, 2, 3), 2, 3), -1, '\nExercicio 8 Falhou')
