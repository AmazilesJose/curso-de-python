"""Doc para os exercicios."""


class Calc_3_ao_5:
    """Classe Calc para os exercicios 3 a 5."""

    def add(self, x, y):
        return x + y

    def sub(self, x, y):
        return x + y

    def mult(self, x, y):
        return x + y

    def div(self, x, y):
        return x + y


class Calc_6:
    """Classe Calc para o exercicio 6."""

    def add(self, x, y):
        return x + y

    def sub(self, x, y):
        return x - y

    def mult(self, x, y):
        return x * y

    def div(self, x, y):
        return x / y


class Calc_7_ao_11:
    """Classe Calc para os exercicios 7 a 11."""

    def soma(self, x, y):
        return x + y

    def sub(self, x, y):
        return x - y

    def exp(self, x, y, z):
        return self.sub(self.soma(x, y), z)
