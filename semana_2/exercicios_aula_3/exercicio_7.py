"""
7.

Dada que exista uma função de soma e uma de subtração.

  Crie uma função chamada ‘exp’ que receba x, y e z. E calcule:

  `f(x, y, z) -> x + y - z`

  Faça o teste dessa função, testando seus inputs e outputs indiretos
"""

from unittest import TestCase
from doc_exercicios import Calc_7_ao_11


class TestExercicio7(TestCase):
    def setUp(self):
        self.c = Calc_7_ao_11()

    def test_exercicio_7(self):
        print('\nTestando Exercicio 7')
        self.assertEqual(self.c.exp(1, 2, 3), 0, '\nExercicio 7 Falhou')
