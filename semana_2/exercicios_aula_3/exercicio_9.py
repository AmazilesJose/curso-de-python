"""
9.

Usando a função ‘exp’ criada no problema 7.
Teste a validação indireta da função de soma usando um dummy
"""

from unittest import TestCase
from doc_exercicios import Calc_7_ao_11


class TestExercicio9(TestCase):
    dum = 0

    def setUp(self):
        self.c = Calc_7_ao_11()
        dum = 0

    def test_exercicio_9(self):
        print('\nTestando Exercicio 9')
        self.assertEqual(self.c.exp(
            self.dum, self.dum, 5), -5, '\nExercicio 9 Falhou')
