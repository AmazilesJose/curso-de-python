"""
6.

Agora que você tem os testes para garantir o comportamaento dos seus métodos.
Você poderia efeuar a correção da classe sem grandes problemas?
"""

from unittest import TestCase
from doc_exercicios import Calc_6


class TestandoExercicio6(TestCase):
    def setUp(self):
        self.c = Calc_6()

    def test_add_2_com_2_retorna_4(self):
        print('Testando soma')
        self.assertEqual(self.c.add(2, 2), 4, 'Falhou: soma')

    def test_sub_2_com_2_retorna_0(self):
        print('Testando sub')
        self.assertEqual(self.c.sub(2, 2), 0, 'Falhou: sub')

    def test_mult_2_com_2_retorna_4(self):
        print('Testando mult')
        self.assertEqual(self.c.mult(2, 2), 4, 'Falhou: mult')

    def test_div_2_com_2_retorna_0(self):
        print('Testando div')
        self.assertEqual(self.c.div(2, 2), 1, 'Falhou: div')
