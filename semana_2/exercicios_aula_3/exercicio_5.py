"""
5.

Data a classe da calculadora. Defina difentes SUTs e os teste individualmente.
"""

from unittest import TestCase
from doc_exercicios import Calc_3_ao_5


class TestCalcAdd(TestCase):
    def test_add_1_com_1_retorna_2(self):
        print('\nTestando Soma')
        self.assertEqual(Calc_3_ao_5().add(1, 1), 2)


class TestCalcSub(TestCase):
    def test_sub_1_com_1_retorna_0(self):
        print('\nTestando Subtração')
        self.assertEqual(Calc_3_ao_5().sub(1, 1), 0)


class TestCalcMult(TestCase):
    def test_mult_1_com_1_retorna_1(self):
        print('\nTestando Multiplicação')
        self.assertEqual(Calc_3_ao_5().mult(1, 1), 1)


class TestCalcDiv(TestCase):
    def test_div_1_com_1_retorna_1(self):
        print('\nTestando Divisão')
        self.assertEqual(Calc_3_ao_5().div(1, 1), 1)
