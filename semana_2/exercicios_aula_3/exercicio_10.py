"""
10.

Usando a função ‘exp’ criada no problema 8.
Teste a validação indireta da função de subtração usando um dummy
"""

from unittest import TestCase
from doc_exercicios import Calc_7_ao_11


class TestExercicio7(TestCase):
    dum = 0

    def setUp(self):
        self.c = Calc_7_ao_11()
        dum = 0

    def test_exercicio_10(self):
        print('\nTestando Exercicio 10')
        self.assertEqual(self.c.exp(
            1, 1, self.dum), 2, '\nExercicio 10 Falhou')
