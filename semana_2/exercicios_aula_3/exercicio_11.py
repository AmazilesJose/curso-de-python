"""
11.

Usando a função ‘exp’.
Faça a checagem dos inputs indiretos da função de soma em um teste.
Em outro teste faça a função dos inputs indiretos da função de sub.

  ```Python
  def exp(x, y, z):
    return sub(soma(x, y), z)
  ```
"""

from unittest import TestCase, mock
from doc_exercicios import Calc_7_ao_11


class TestExercicio7(TestCase):

    def setUp(self):
        self.c = Calc_7_ao_11()

    def test_exercicio_11_soma(self):
        print('\n Testando Exercicio 11 Soma')
        with mock.patch('doc_exercicios.Calc_7_ao_11.soma') as Mock:
            self.c.exp(1, 2, 3)
        Mock.assert_called_with(1, 2)

    def test_exercicio_11_sub(self):
        print('\n Testando Exercicio 11 Sub')
        with mock.patch('doc_exercicios.Calc_7_ao_11.sub') as Mock:
            self.c.exp(1, 2, 3)
        Mock.assert_called_with(3, 3)
