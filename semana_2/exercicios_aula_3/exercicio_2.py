"""
2.

Dada a função de soma

```Python
def soma(x, y):
    return x + y
```

Faça assertivas de valores que você acredita serem coerentes com a função.
"""


def soma(x, y):
    return x + y


assert soma(1, 1) == 2
assert soma(1.2, 1) == 2.2
assert soma(soma(1, 1), 1) == 3
assert soma("tes", "te") == "teste"
