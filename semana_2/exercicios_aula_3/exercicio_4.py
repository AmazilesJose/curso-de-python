"""
4.

Crie sua primeira suite de testes para a classe da calculadora.
"""

from unittest import TestCase
from doc_exercicios import Calc_3_ao_5


class TestandoExercicio3(TestCase):
    def setUp(self):
        self.c = Calc_3_ao_5()

    def test_add_2_com_2_retorna_4(self):
        print('Testando soma')
        self.assertEqual(self.c.add(2, 2), 4, 'Deu ruim')

    def test_sub_2_com_2_retorna_0(self):
        print('Testando soma')
        self.assertEqual(self.c.sub(2, 2), 0, 'Deu ruim')
