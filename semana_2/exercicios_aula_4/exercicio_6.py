from unittest import TestCase, main
from typing import Callable

def get_users():
    l = [
        (12, 'joão', 'email'),
        (80, 'pedro', 'email'),
        (100, 'jose', 'email'),
        (120, 'roberto', 'email'),
        (123, 'matheus', 'email'),
        (765, 'paulo', 'email')
      ]
    return l


def filter_users(funcao: Callable):
    Lista = funcao
    nova_l= []
    for user in Lista:
        if user[0] > 100:
            nova_l.append(user)
    return nova_l

class TestExercicio6(TestCase):
    def test_stub_get_users(self):
        saida = [(120, 'roberto', 'email'), (123, 'matheus', 'email'), (765, 'paulo', 'email')]
        self.assertEqual(filter_users(get_users()), saida)

if __name__ == '__main__':
    main(verbosity=2)
