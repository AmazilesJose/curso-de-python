from unittest import TestCase, main
from exercicio_6 import filter_users, get_users

class TesteDoExercicio7(TestCase):
    def teste_do_exercicio_6_deve_retornar_apenas_usuarios_com_id_maior_que_100(self):
        saida = [(120, 'roberto', 'email'), (123, 'matheus', 'email'), (765, 'paulo', 'email')]
        self.assertEqual(filter_users(get_users()), saida)


if __name__ == '__main__':
    main(verbosity=2)
