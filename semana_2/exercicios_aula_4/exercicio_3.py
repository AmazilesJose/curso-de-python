from unittest import TestCase, main
from collections import Counter

def conta_letras(string):
    return Counter(string)


class TestFreqLetras(TestCase):
    def test_freq(self):
        entrada = 'asd'
        saida = {'a': 1, 's': 1, 'd': 1}
        self.assertEqual(conta_letras(entrada), saida)




if __name__ == '__main__':
    main(verbosity = 2)
