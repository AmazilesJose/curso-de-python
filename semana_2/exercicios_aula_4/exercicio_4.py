from unittest import TestCase, main

def spliteira(string):
    return string.split()


class TestSplit(TestCase):
    def test_retorna_python(self):
        entrada = 'Python'
        saida = ['Python']
        self.assertEqual(spliteira(entrada), saida)
    def test_retorna_python_love(self):
        entrada = 'Python love'
        saida = ['Python', 'love']
        self.assertEqual(spliteira(entrada), saida)

    def test_retorna_python_e_foda(self):
        entrada = 'Python é foda'
        saida = ['Python', 'é', 'foda']
        self.assertEqual(spliteira(entrada), saida)

    def test_spliteira_retorna_com_barra_n(self):
        entrada = 'Pyt\nhon'
        saida = ['Pyt', 'hon']
        self.assertEqual(spliteira(entrada), saida)

    def test_spliteira_retorna_com_barra_b(self):
        entrada = 'Pyt\bhon'
        saida = ['Py', 'hon']
        self.assertEqual(spliteira(entrada), saida)



if __name__ == '__main__':
    main(verbosity = 2)
