from unittest import TestCase, main, mock, TestLoader, TextTestRunner
from doc_exercicio_8 import Doc

class TestAchaPizza(TestCase):
    def teste_retorna_pizzas_com_valor_menor_ou_igual_ao_valor_que_o_usuario_quer_gastar(self):
        saida = [('muçarela', 15.0), ('calabresa', 7.5)]

        pizzas = [('muçarela', 15.0),
                  ('calabresa', 7.5),
                  ('frango e catupiry', 45.7),
                  ('margerita', 20.0),
                  ('brocolis', 48.45)]

        with mock.patch('doc_exercicio_8.Doc.sabores_de_pizza',
            return_value = pizzas) as sabores_de_pizza_mocked:
            resultado_mock = Doc.pizza_ideal(15.50)

        self.assertEqual(resultado_mock, saida)
