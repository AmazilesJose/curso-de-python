from unittest import TestCase, main
from hypothesis import given, example
from hypothesis.strategies import lists, text
from exercicio_3 import conta_letras
from exercicio_4 import spliteira


def func(string: str):
    return conta_letras(spliteira(string))



class TestExercicio5(TestCase):
    def test_com_string(self):
        entrada = 'oi'
        saida = {'oi': 1}
        self.assertEqual(func(entrada), saida)



if __name__ == '__main__':
    main(verbosity = 2)
