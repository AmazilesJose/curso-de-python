from unittest import TestCase, main
from hypothesis import given, example
from hypothesis.strategies import text
from pedrapapeltesoura import jogo_jokenpo


class TestJokenpoComHypothesis(TestCase):

    @given(text(), text())
    @example('papel', 'pedra')
    def test_jokenpo_papel_pedra(self, entrada_1, entrada_2):
        self.assertEqual(jogo_jokenpo(entrada_1, entrada_2), jogo_jokenpo(entrada_2, entrada_1))

    @given(text(), text())
    @example('papel', 'tesoura')
    def test_jokenpo_papel_tesoura(self, entrada_1, entrada_2):
        self.assertEqual(jogo_jokenpo(entrada_1, entrada_2), jogo_jokenpo(entrada_2, entrada_1))


    @given(text(), text())
    @example('pedra', 'tesoura')
    def test_jokenpo_pedra_tesoura(self, entrada_1, entrada_2):
        self.assertEqual(jogo_jokenpo(entrada_1, entrada_2), jogo_jokenpo(entrada_2, entrada_1))

    @given(text(), text())
    @example('papel', 'papel')
    def test_jokenpo_empate(self, entrada_1, entrada_2):
        self.assertEqual(jogo_jokenpo(entrada_1, entrada_2), jogo_jokenpo(entrada_2, entrada_1))

class TestJokenpoSemHypothesis(TestCase):
    def test_jokenpo_papel_pedra(self):
        entrada_1 = 'papel'
        entrada_2 = 'pedra'
        self.assertEqual(jogo_jokenpo(entrada_1, entrada_2), jogo_jokenpo(entrada_2, entrada_1))

    def test_jokenpo_papel_tesoura(self):
        entrada_1 = 'papel'
        entrada_2 = 'tesoura'
        self.assertEqual(jogo_jokenpo(entrada_1, entrada_2), jogo_jokenpo(entrada_2, entrada_1))

    def test_jokenpo_pedra_tesoura(self):
        entrada_1 = 'pedra'
        entrada_2 = 'tesoura'
        self.assertEqual(jogo_jokenpo(entrada_1, entrada_2), jogo_jokenpo(entrada_2, entrada_1))

    def test_jokenpo_empate(self):
        entrada_1 = 'papel'
        entrada_2 = 'papel'
        self.assertEqual(jogo_jokenpo(entrada_1, entrada_2), jogo_jokenpo(entrada_2, entrada_1))
if __name__ == '__main__':
     main(verbosity = 2)
